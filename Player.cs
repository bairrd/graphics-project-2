﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Input;
using Windows.UI.Input;
using Windows.UI.Core;

namespace Project2
{
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;
    // Player class.
    class Player : GameObject
    {
        //private float speed = 0.006f;
        private float projectileSpeed = 200;
        private double health = 100;
        private float fireCooldown = 0; //milliseconds
        private Model actualModel;
        private Vector3 rotation;

        private bool spinning;
        private Vector3 spin;

        private bool dead;

        public Player(Project2Game game)
        {
            this.game = game;
            type = GameObjectType.Player;
            myModel = game.assets.GetModel("player", CreatePlayerModel);
            
            //testing model
            actualModel = game.Content.Load<Model>("shuttle");
            BasicEffect.EnableDefaultLighting(actualModel, true);
            rotation = Vector3.Zero;

            //fancy stuff
            dead = false;
            spinning = false;
            spin = Vector3.Zero;

            pos = new SharpDX.Vector3(0, game.boundaryBottom + 0.5f, 0);
            GetParamsFromModel();
        }

        public MyModel CreatePlayerModel()
        {
            return game.assets.CreateTexturedCube("player.png", 0.7f);
        }

        // Method to create projectile texture to give to newly created projectiles.
        private MyModel CreatePlayerProjectileModel()
        {
            return game.assets.CreateTexturedCube("player projectile.png", new Vector3(0.3f, 0.2f, 0.25f));
        }

        // Shoot a projectile.
        private void fire()
        {
            if (fireCooldown <= 0)
            {
                game.Add(new Projectile(game,
                    game.assets.GetModel("player projectile", CreatePlayerProjectileModel),
                    pos,
                    new Vector3(0, 0, projectileSpeed),
                    GameObjectType.Enemy
                ));
                fireCooldown = 70f; //milliseconds
            }
        }

        // Frame update.
        public override void Update(GameTime gameTime)
        {
            //cheating for a death animation
            if (health < 0) dead = true;
            if (dead)
            {
                game.mainPage.dead();
                spin.Y += 0.3f;
                game.Add(new Explosion(this.game, this.pos+game.random.NextVector3(-Vector3.One,Vector3.One)));
                if (spin.Y >= Math.PI * 16)
                {
                    //game.mainPage.GameOver();
                    game.Exit();
                    game.Dispose();
                    App.Current.Exit();
                }
                basicEffect.World = Matrix.RotationY(spin.Y) * Matrix.Translation(pos);
                return;
            }

            if (game.keyboardState.IsKeyDown(Keys.Space)) { fire(); }

            // Determine velocity based on accelerometer reading (if accelerometer exists), or just move on arrow key input.

            if (game.accelerometerExists)
            {
                pos.X += (float) game.accelerometerReading.AccelerationX;
                rotation.Y += (float)game.accelerometerReading.AccelerationX;
                pos.Y -= (float)game.accelerometerReading.AccelerationZ;
                rotation.X += (float)game.accelerometerReading.AccelerationZ;
            }
            else
            {
                if (game.keyboardState.IsKeyDown(Keys.Left))
                {
                    pos.X -= 0.15f;
                    rotation.Z += 0.1f;
                }
                if (game.keyboardState.IsKeyDown(Keys.Right))
                {
                    pos.X += 0.15f;
                    rotation.Z -= 0.1f;
                }
                if (game.keyboardState.IsKeyDown(Keys.Up))
                {
                    pos.Y += 0.15f;
                    rotation.X -= 0.1f;
                }
                if (game.keyboardState.IsKeyDown(Keys.Down))
                {
                    pos.Y -= 0.15f;
                    rotation.X += 0.1f;
                }
                if (game.keyboardState.IsKeyDown(Keys.P))
                {
                    game.mainPage.PauseP();
                }
            }
            /*
            else if (game.keyboardState.IsKeyDown(Keys.Q))
            {
                pos.Z += 0.1f;
            }
            else if (game.keyboardState.IsKeyDown(Keys.A))
            {
                pos.Z -= 0.1f;
            } */
            
            // pull back rotation
            rotation = rotation * 0.75f;

            // Keep within the boundaries.
            if (pos.X < game.boundaryLeft) { pos.X = game.boundaryLeft; }
            if (pos.X > game.boundaryRight) { pos.X = game.boundaryRight; }
            if (pos.Y < game.boundaryBottom) { pos.Y = game.boundaryBottom; }
            if (pos.Y > game.boundaryTop) { pos.Y = game.boundaryTop; }

            fireCooldown -= gameTime.ElapsedGameTime.Milliseconds;

            if (spinning)
            {
                spin.Y += 0.2f;
                rotation = spin;
                if (spin.Y >= Math.PI * 4)
                {
                    spinning = false;
                    rotation = Vector3.Zero;
                    spin = Vector3.Zero;
                }
            }

            basicEffect.World = Matrix.RotationY(rotation.Y) * Matrix.RotationX(rotation.X) * Matrix.RotationZ(rotation.Z) * Matrix.Translation(pos);
        }

        // React to getting hit by an enemy bullet.
        public void Hit(double damage)
        {
            //game.Exit();
            this.health = this.health - damage;
            this.spinning = true;
            //game.started = false;
        }

        public override void Tapped(GestureRecognizer sender, TappedEventArgs args)
        {
            if (game.accelerometerExists)
            {
                fire();
            }
        }

        public override void OnManipulationUpdated(GestureRecognizer sender, ManipulationUpdatedEventArgs args)
        {
            if (game.accelerometerExists)
            {
                pos.X += (float)args.Delta.Translation.X / 100;
                pos.Y += (float)args.Delta.Translation.Y / 100;
            }
        }

        public override void Draw(GameTime gametime)
        {
            actualModel.Draw(game.GraphicsDevice, basicEffect.World, game.camera.View, game.camera.Projection);
        }

        public double getPlayerHP()
        {
            double playerhealth = this.health;            
            return playerhealth;
        }

    }
}