﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using SharpDX;

namespace Project2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        private readonly Project2Game game;

        public MainPage()
        {
            InitializeComponent();
            game = new Project2Game(this);
            game.Run(this);
            this.txtResume.Visibility = Visibility.Collapsed;
            this.txtDEAD.Visibility = Visibility.Collapsed;
        }


        public void UpdateScore(int score)
        {
            txtScore.Text = "Score: " + score.ToString();
        }

        public void UpdateHealth(double health)
        {
            player_hp.Text = "Health: " + health;
        }

        public void GameOver()
        {
            game.started = false;
            cmdStart.Visibility = Visibility.Visible;
        }

        public void Pause(object sender, RoutedEventArgs e)
        {
            if (game.started == true)
            {
                game.started = false;
                this.txtResume.Visibility = Visibility.Visible;
            }
            else
            {
                game.started = true;
                this.txtResume.Visibility = Visibility.Collapsed;
            }
        }

        public void PauseP()
        {
            if (game.started == true)
            {
                game.started = false;
                this.txtResume.Visibility = Visibility.Visible;
            }
            else
            {
                game.started = true;
                this.txtResume.Visibility = Visibility.Collapsed;
            }
        }

        private void StartGame(object sender, RoutedEventArgs e)
        {

            game.started = true;
            cmdStart.Visibility = Visibility.Collapsed;
        }

        public void dead()
        {
            this.txtDEAD.Visibility = Visibility.Visible;
        }

        private void changeDifficulty(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            if (game != null) { game.difficulty = (float)e.NewValue; }
        }
    }
}
