﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;

namespace Project2
{
    using SharpDX.Toolkit.Graphics;


    // Enemy Controller class.
    class LandscapeController : GameObject
    {
        private int maxSegments = 4;
        private int maxObstacles = 12;

        public LandscapeController(Project2Game game)
        {
            Random randObj = new Random();
            this.game = game;
            initialSetup(randObj);
        }
    
        private void initialSetup(Random randObj) 
        {
            for (int i=1; i <= maxSegments; i++) 
            {
                game.Add(new Landscape(game, new Vector3(0, -8, i*600 - 400)));
            }
            for (int i = 1; i <= maxObstacles; i++)
            {
                float xValue = -2.1f + (float) randObj.NextDouble()*5;
                while (xValue > -0.9f && xValue < 0.9f)
                {
                    xValue = -2.1f + (float)randObj.NextDouble() * 5;
                }
                game.Add(new Obstacle(game, new Vector3(xValue, -6f, i*200 + 400)));
            }
        }

        public override void Update(GameTime gameTime)
        {
            
        }
    }


}