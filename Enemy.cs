﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Toolkit;
using SharpDX;
namespace Project2
{
    // Enemy class
    // Basically just shoots randomly, see EnemyController for enemy movement.
    using SharpDX.Toolkit.Graphics;
    class Enemy : GameObject
    {
        private float projectileSpeed = 40f;
        private Model actualModel;
        private double x_velocity = 0;
        private double y_velocity = 0;
        private double z_velocity = 0;

        float fireTimer;
        float fireWaitMin = 200;
        float fireWaitMax = 7500;

        public Enemy(Project2Game game, Vector3 pos, double initial_x_velocity, double initial_y_velocity)
        {
            this.game = game;
            type = GameObjectType.Enemy;
            myModel = game.assets.GetModel("ship", CreateEnemyModel);

            actualModel = game.Content.Load<Model>("ORCO1");
            BasicEffect.EnableDefaultLighting(actualModel, true);

            this.pos = pos;
            this.x_velocity = initial_x_velocity;
            this.y_velocity = initial_y_velocity;
            this.z_velocity = game.random.NextDouble(1.0, 5.0);

            setFireTimer();
            GetParamsFromModel();
        }

        private void setFireTimer()
        {
            fireTimer = fireWaitMin + (float)game.random.NextDouble() * (fireWaitMax - fireWaitMin);
        }

        public MyModel CreateEnemyModel()
        {
            return game.assets.CreateTexturedCube("enemy.png", 0.5f);
        }

        private MyModel CreateEnemyProjectileModel()
        {
            return game.assets.CreateTexturedCube("enemy projectile.png", new Vector3(0.2f, 0.2f, 0.4f));
        }

        public override void Update(GameTime gameTime)
        {
            float elapsed_time = gameTime.ElapsedGameTime.Milliseconds / 1000f;
            fireTimer -= gameTime.ElapsedGameTime.Milliseconds * game.difficulty;
            float new_x = this.pos.X + elapsed_time * 1f * (float)this.x_velocity;
            float new_y = this.pos.Y + elapsed_time * 0.4f * (float)this.y_velocity;

            if (new_x < game.boundaryLeft || new_x > game.boundaryRight)
            {
                x_velocity = -x_velocity;
                new_x = this.pos.X; //reset
            }

            if (new_y > game.boundaryTop || new_y < game.boundaryBottom)
            {
                y_velocity = -y_velocity;
                new_y = this.pos.Y;
            }

            this.pos.X = new_x;
            this.pos.Y = new_y;
            this.pos.Z = this.pos.Z - elapsed_time * 10f * (float)this.z_velocity;
            checkForCollisions();

            if (pos.Z + 20 < game.boundaryBehindCamera)
            {
                game.Remove(this);
            }

            if (fireTimer < 0)
            {
                fire();
                setFireTimer();
            }
            basicEffect.World = Matrix.Translation(pos);
        }

        public override void Draw(GameTime gameTime)
        {
            actualModel.Draw(game.GraphicsDevice, basicEffect.World, game.camera.View, game.camera.Projection);
        }

        private void fire()
        {
            game.Add(new Projectile(game,
                game.assets.GetModel("enemy projectile", CreateEnemyProjectileModel),
                new Vector3(this.pos.X+0.8f, this.pos.Y, this.pos.Z),
                new Vector3(0.0f, 0, -projectileSpeed),
                GameObjectType.Player
            ));
        }

        public void Hit()
        {
            game.score += 10;
            game.Remove(this);
        }
        private void checkForCollisions()
        {
            foreach (var obj in game.gameObjects)
            {
                if (obj.type == GameObjectType.Player && ((((GameObject)obj).pos - pos).LengthSquared() <=
                    Math.Pow(((GameObject)obj).myModel.collisionRadius + this.myModel.collisionRadius, 2)))
                {
                    // Cast to object class and call Hit method.
                    switch (obj.type)
                    {
                        case GameObjectType.Player:
                            ((Player)obj).Hit(15);
                            game.Remove(this);
                            break;
                    }

                    // Destroy self.
                    //game.Remove(this);
                }
            }
        }

    }
}
