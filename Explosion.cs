﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;

namespace Project2
{
    using SharpDX.Toolkit.Graphics;
    // Explosion for projectile hit
    class Explosion : GameObject
    {
        private int life = 2000;

        // Constructor.
        public Explosion(Project2Game game, Vector3 pos)
        {
            this.game = game;
            this.pos = pos;
            this.myModel = game.assets.GetModel("explosion", CreateExplosion);
            GetParamsFromModel();
        }

        public MyModel CreateExplosion()
        {
            return game.assets.CreateExplosion("explosion0.png", 1.0f);
        }

        // Frame update method.
        public override void Update(GameTime gameTime)
        {
            basicEffect.World = Matrix.Translation(pos);

            life -= gameTime.ElapsedGameTime.Milliseconds;

            if (life <= 0) game.Remove(this);
        }
    }
}
