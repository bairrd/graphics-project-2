Graphics & Interaction: Project 2

Group members:
Thomas Grundy - tgrundy@student.unimelb.edu.au
Budi Sugianto Utomo - bsjap@student.unimelb.edu.au
Ben Chong - bkchong@student.unimelb.edu.au

1. What the application does:
Our application is a game in which the player controls a flying spaceship. The object of the game is to 
survive as long as possible, whilst the player flys through a randomly generated environment of enemies, 
which fire at the player, and towers, which the player must fly around.

2. How to use it (especially the natural user interface aspects):
If playing on a laptop, the arrow keys control player movement, whilst spacebar fires. On a tablet, 
tilting the tablet moves the player and touching on the screen fires.

3. How you modelled objects and entities:
Our objects were all sources from opengameart.org, with appropriate "open" licenses, resized appropriately in
Blender, and then imported into our game and rendered for the player. In terms of the code itself, each entity
was given its own class, e.g. Obstacle or Enemy. This let us customize and iterate on them throughout development.

4. How you handled graphics and camera motion:
We render the camera statically, and scroll the enemies and landscape towards the camera, to give the impression of 
forward momentum of the player. Graphically, most of our models had normal maps, that allowed us to light highlights on the
models.

5. A statement about any code/APIs you have sourced/used from the internet that is not
your own:
All of our code is our own, or based off the workshop code provided to us through the subject. Our models are all
appropriately sourced from opengameary.org

Logo built with flamingtext.com

External models:

mage_tower: http://opengameart.org/content/mage-tower
Public domain license.
Attribution Instructions: 
Attribute this site, opengameart.org!
Thanks, opengameart.org and djonvincent!

shuttle: http://opengameart.org/content/shuttle-2
Creative Commons license.
Attribution Instructions: 
Please attribute me as "Kenten Fina".
Thanks, Kenten Fina!

hellfire: http://opengameart.org/content/hellfire-anti-tank-missile
GPL V2 license.
No attribution instructions.
Texture is modified to look more terrible
Thanks, Mystic Mike (Mike Hosker)!

monster-face: http://opengameart.org/content/monster-face
Creative Commons License
No attribution instructions.
Thanks, killyoverdrive!

grass texture: http://opengameart.org/content/fastgras01
Public domain license.
No attribution instructions.
Thanks, Duion!

sky backdrop: http://opengameart.org/content/sky-backdrop
Creative Commons License
No attribution instructions.
thanks bart

explosion0.png: http://opengameart.org/content/explosions
Public Domain license
Thanks GameProgrammingSlave