﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Toolkit;
using SharpDX;
namespace Project2
{
    // Landscape class
    // Generates the scrolling landscape the player and enemies are flying over.
    using SharpDX.Toolkit.Graphics;
    class Landscape : GameObject
    {
        private float scrollSpeed;

        public Landscape(Project2Game game, Vector3 pos)
        {
            this.game = game;
            this.scrollSpeed = game.scrollSpeed;
            type = GameObjectType.Landscape;
            myModel = game.assets.GetModel("landscape", CreateLandscapeModel);
            this.pos = pos;
            GetParamsFromModel();
        }

        private MyModel CreateLandscapeModel()
        {
            return game.assets.CreateFlatLandscape("landscape.png", 1.2f);
            //return game.assets.CreateTexturedCube("landscape.png", 1.2f);

            //TODO: Randomly generate different shapes/buildings/arches and place them on the landscape.
        }

        public override void Update(GameTime gameTime) 
        {
            //Scroll the landscape towards the camera.
            pos.Z -= scrollSpeed;
            basicEffect.World = Matrix.Translation(pos);
            

            if (pos.Z + 300 < game.boundaryBehindCamera)
            {

                pos.Z = 2050;
            }

        }
    }
}