﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Toolkit;
using SharpDX;
namespace Project2
{
    // Sky class
    // Generates the sky backdrop.
    using SharpDX.Toolkit.Graphics;
    class Sky : GameObject
    {
        public Sky(Project2Game game)
        {
            this.game = game;
            type = GameObjectType.Landscape;
            myModel = game.assets.GetModel("sky", CreateSkyModel);
            this.pos = new Vector3(0,0,00);
            GetParamsFromModel();
        }

        public Sky(Project2Game game, Vector3 pos)
        {
            this.game = game;
            type = GameObjectType.Landscape;
            myModel = game.assets.GetModel("sky", CreateSkyModel);
            this.pos = pos;
            GetParamsFromModel();
        }

        private MyModel CreateSkyModel()
        {
            return game.assets.CreateFlatBackdrop("sky1.png", 1.2f);
            //return game.assets.CreateTexturedCube("landscape.png", 1.2f);

            //TODO: Randomly generate different shapes/buildings/arches and place them on the landscape.
        }

        public override void Update(GameTime gameTime) 
        {
            basicEffect.World = Matrix.Translation(pos);
        }
    }
}