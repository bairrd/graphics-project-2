﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;

namespace Project2
{
    using SharpDX.Toolkit.Graphics;

    
    // Enemy Controller class.
    class EnemyController : GameObject
    {
        // Spacing and counts.
        private double spawntimer = 750f; //controls how often enemies can spawn
        private int maxenemyCount = 20; //controls maximum number of enemies
        private double spawntime = 750f; //is the time that the spawn timer is reset to

        // Timing and movement
        Random random = new Random();

        // Constructor.
        public EnemyController(Project2Game game)
        {
            this.game = game;
            nextWave();
        }

        // Set up the next wave.
        private void nextWave()
        {
            //rows += 1;
            //stepWait = 1000f / (1 + rows / 3f);
            spawntimer = spawntime; //reset the spawntimer
            createEnemies();
            //stepRight = true;
        }

        // Create a grid of enemies for the current wave.
        private void createEnemies()
        {
            float y = random.NextFloat(game.boundaryBottom, game.boundaryTop);
            //float y = game.boundaryTop;
            float x = random.NextFloat(game.boundaryLeft, game.boundaryRight);
            //float x = game.boundaryLeft;
            //DO IT THRICE
            game.Add(new Enemy(game, new Vector3(x, y, 200), random.Next(-3, 3), random.Next(-1, 1)));
            game.Add(new Enemy(game, new Vector3(x, y, 200), random.Next(-3, 3), random.Next(-1, 1)));
            game.Add(new Enemy(game, new Vector3(x, y, 200), random.Next(-3, 3), random.Next(-1, 1)));
            
            // DO IT TWICE
            y = random.NextFloat(game.boundaryBottom, game.boundaryTop);
            //float y = game.boundaryTop;
            x = random.NextFloat(game.boundaryLeft, game.boundaryRight);
            //float x = game.boundaryLeft;
            game.Add(new Enemy(game, new Vector3(x, y, 200), random.Next(-3, 3), random.Next(-1, 1)));
            game.Add(new Enemy(game, new Vector3(x, y, 200), random.Next(-3, 3), random.Next(-1, 1)));
            game.Add(new Enemy(game, new Vector3(x, y, 200), random.Next(-3, 3), random.Next(-1, 1)));
        }

        // Frame update method.
        public override void Update(GameTime gameTime)
        {
            spawntimer -= gameTime.ElapsedGameTime.Milliseconds * game.difficulty;
            step();
            //stepTimer = stepWait;

            // Invoke next wave once current one has ended.
           // if (allEnemiesAreDead())
           //{
            //    nextWave();
           //
            if ((getCurrentNumEnemies() < maxenemyCount) && spawntimer < 0)
            {
                nextWave();
            }

        }

        // Return whether all enemies are dead or not.
        private bool allEnemiesAreDead()
        {
            return game.Count(GameObjectType.Enemy) == 0;
        }

        private int getCurrentNumEnemies()
        {
            return game.Count(GameObjectType.Enemy);
        }

        //
        private void step()
        {
            //this code moves all enemies
            foreach (var obj in game.gameObjects)
            {
                if (obj.type == GameObjectType.Enemy)
                {
                    Enemy enemy = (Enemy)obj;
                    

                    //if (stepRight)
                    //{
                    //    enemy.pos.X += stepSize;
                    //    if (enemy.pos.X > game.boundaryRight) { stepDownNeeded = true; }
                    //}
                    //else
                    //{
                    //    enemy.pos.X -= stepSize;
                    //    if (enemy.pos.X < game.boundaryLeft) { stepDownNeeded = true; }
                    //}
                }
            }

        }

        // Step all enemies down one.

        // Method for when the game ends.
        private void gameOver()
        {
            game.Exit();
        }
    }
}