﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;

namespace Project2
{
    class Obstacle : GameObject
    {
        private float scrollSpeed;
        private Model actualModel;
        private Random randObj;

        public Obstacle(Project2Game game, Vector3 pos)
        {
            this.game = game;
            this.scrollSpeed = game.scrollSpeed;
            type = GameObjectType.Landscape;
            myModel = game.assets.GetModel("ship", CreateObstacleModel);
            actualModel = game.Content.Load<Model>("mage_tower");
            myModel.collisionRadius = 2f;
            //myModel.collisionRadius = actualModel.CalculateBounds().Radius;
            BasicEffect.EnableDefaultLighting(actualModel, true);
            this.pos = pos;
            this.randObj = new Random();
            GetParamsFromModel();
        }

        private MyModel CreateObstacleModel()
        {
            return game.assets.CreateTexturedCube("enemy.png", 0.5f);
        }

        public override void Draw(GameTime gametime)
        {
            actualModel.Draw(game.GraphicsDevice, basicEffect.World, game.camera.View, game.camera.Projection);
        }

        public override void Update(GameTime gameTime)
        {
            pos.Z -= scrollSpeed;
            checkForCollisions();
            basicEffect.World = Matrix.Translation(pos);

            if (pos.Z + 300 < game.boundaryBehindCamera)
            {
                float xValue = game.random.NextFloat(game.boundaryLeft,game.boundaryRight);

                game.Add(new Obstacle(game, new Vector3(xValue, -6f, 2200)));
                game.Remove(this);
            }

        }

        private void checkForCollisions()
        {
            Vector3 newPos = new Vector3(pos.X, pos.Y + 4f, pos.Z);
            foreach (var obj in game.gameObjects)
            {
                if ((obj.type == GameObjectType.Player || obj.type == GameObjectType.Enemy) &&
                    ((((GameObject)obj).pos - pos).LengthSquared() <=
                    Math.Pow(((GameObject)obj).myModel.collisionRadius + this.myModel.collisionRadius, 2)
                    || (((GameObject)obj).pos - newPos).LengthSquared() <=
                    Math.Pow(((GameObject)obj).myModel.collisionRadius + this.myModel.collisionRadius, 2)
                    ))
                {
                    // Cast to object class and call Hit method.
                    switch (obj.type)
                    {
                        case GameObjectType.Player:
                            ((Player)obj).Hit(20);
                            game.Remove(this);
                            break;
                        case GameObjectType.Enemy:
                            //((Enemy)obj).Hit();
                            break;
                    }

                    // Destroy self.
                    //game.Remove(this);
                }
            }
        }
    }
}
